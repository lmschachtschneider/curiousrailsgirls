class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|

      t.timestamps
      t.string :name 
      t.text :description
    end
  end
end
